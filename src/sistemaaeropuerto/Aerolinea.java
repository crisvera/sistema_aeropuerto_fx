package sistemaaeropuerto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Clase que representra una aerolinea.
 */
public class Aerolinea implements Serializable {
    
    String nombre;
    ArrayList<Avion> lista_de_aviones;
    ArrayList<Vuelo> vuelos;
    HashMap<Integer, Avion> gates;

    Aerolinea(String nombre) {
        this.nombre = nombre;
        lista_de_aviones = new ArrayList();
        vuelos = new ArrayList();
    }

    public void agregar_avion(Avion avion) {
        lista_de_aviones.add(avion);
    }

    public void agregar_vuelo(Vuelo vuelo) {
        vuelos.add(vuelo);
    }

    public boolean verificar_vuelo(Vuelo vuelo) {
        return false;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
    
    
}

package sistemaaeropuerto;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Clase temporal de transicion entre el almacenamiento en archivos txt vs
 * almacenamiento a traves de la serializacion.
 * <p>
 * Contiene varias funciones utilitarias de lectura y procesamiento de archivos.
 */
public class ProcesadorDeArchivos {
    
    private ProcesadorDeArchivos() {}
    
    /**
     * Lee una lista csv con la informacion de todos los empleados y la usa para
     * obtener su informacion y crear los apropiadas objetos.
     *
     * @return un ArrayList con todos los objetos empleado.
     * @throws IOException si los archivos de texto son borrados o se les cambia
     * la dirreccion
     */
    public static ArrayList<Empleado> cargarEmpleados() {

        ArrayList<Empleado> empleados = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("usuarios.txt"))) {
            // Salta primera linea porque no contiene info relevante
            br.readLine();

            String linea;
            while ((linea = br.readLine()) != null) {
                empleados.add(procesaEmpleadoDesdeStringCSV(linea));
            }
        } catch (IOException e) {
            System.out.println(e);
        }

        return empleados;
    }
    
    /**
     * De una linea de texto obtenida en un archivo con todos los empleados en
     * formato CSV, separa los campos para generar el empleado apropiado
     *
     * @param linea un String con valores separados por coma
     * @return un objeto empleado
     */
    public static Empleado procesaEmpleadoDesdeStringCSV(String linea) {

        String cedula, nombre, apellido, email, usuario, clave, rol, aerolinea, departamento;
        Empleado empleado;

        ArrayList<String> valores = new ArrayList<String>(Arrays.asList(linea.split(",")));

        cedula = valores.get(0);
        nombre = valores.get(1);
        apellido = valores.get(2);
        email = valores.get(3);
        usuario = valores.get(4);
        clave = valores.get(5);
        rol = valores.get(6).toUpperCase();
        aerolinea = valores.get(7);
        departamento = valores.get(8);

        switch (rol) {
            case "A":
                empleado = new Administrador(cedula, email, departamento, usuario, clave, rol);
                break;
            case "C":
                // aqui hay un error. Encontrar la forma de obtener objetos Aerolinea apropiados
                empleado = new Cajero(cedula, email, departamento, usuario, clave, new Aerolinea(aerolinea), rol);
                break;
            case "P":
                empleado = new Planificador(cedula, email, departamento, usuario, clave, rol,new Aerolinea(aerolinea));
                break;
            default:
                empleado = null;
        }

        return empleado;

    }
    
    public static ArrayList<Aerolinea> cargarAerolineas() {
        ArrayList<Aerolinea> aerolineas = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader("aerolineas.txt"))) {
            // Salta primera linea porque no contiene info relevante
            br.readLine();

            String linea;
            while ((linea = br.readLine()) != null) {
                aerolineas.add(procesaAerolineaDesdeStringCSV(linea));
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        return aerolineas;
    }
    
    /**
     * De una linea de texto obtenida en un archivo con todas las aerolineas en
     * formato CSV, separa los campos para generar las aerolineas apropiadas
     *
     * @param linea un String con valores separados por coma
     * @return un objeto aerolinea
     */
    private static Aerolinea procesaAerolineaDesdeStringCSV(String linea) {
        String nombre = linea;
        return new Aerolinea(nombre);
    }
}

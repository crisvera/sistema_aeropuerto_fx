/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaaeropuerto;

import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import static sistemaaeropuerto.Cajero.GetStringArray;

/**
 *
 * @author acast
 */
public class PanelVuelos implements Runnable {
    String name;
    int time;
    ListView lv;
    Aerolinea a;
    private Vuelo ultimo_vuelo;
    public PanelVuelos(Aerolinea a,ListView lv){
        time=3*999;
        this.lv = lv;     
        ObservableList<String> items =FXCollections.observableArrayList (mostrar_vuelos_primera(a));
        lv.setItems(items);
    }
    public void run(){
        try{
        // Update the TextArea when the selected season changes
                
                Thread.sleep(time);
                ObservableList<String> items =FXCollections.observableArrayList (mostrar_vuelos(a));
                 lv.setItems(items);
        }catch(Exception e){
            System.out.println("error en theatd"); 
            System.out.println(e);
        }
    }
      public String[] mostrar_vuelos_primera(Aerolinea a) { 
          System.out.println("cargo vuelos");
            ArrayList<String> str = new ArrayList();
            int cont = 0;
            for (Vuelo v : a.vuelos) {
                if(cont <=9){
                    str.add(v.toString());
                }
                if(cont == 9){
                    this.ultimo_vuelo = v;
                }
                cont++;
                
            }
            return GetStringArray(str);
    }
           public String[] mostrar_vuelos(Aerolinea a) { 
               System.out.println("segunda");
            ArrayList<String> str = new ArrayList();
            int cont = 10;
            for (Vuelo v : a.vuelos) {
                if (v.equals(this.ultimo_vuelo) && cont>9){
                    cont=0;
                }
                if(cont <=9){
                    str.add(v.toString());
                }
                if(cont == 9){
                    this.ultimo_vuelo = v;
                }
                cont++;
                
            }
            return GetStringArray(str);
    }
      
      
}


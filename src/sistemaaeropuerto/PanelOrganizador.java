package sistemaaeropuerto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 * Esta clase contiene todos los paneles a usar.
 * <p>
 * Cada panel tiene la abilidad de cambiar a uno diferente. El uso de esto
 * permite a la aplicacion navegar entre diferentes pantallas.
 */
public class PanelOrganizador {

    private GridPane menuLogin;
    private GridPane menuAdmin;
    private GridPane menuCajero;
    private GridPane menuPlanificador;
   // private Sistema sistema = inicializarSistemaConSerializacion();
    private Sistema sistema = inicializarSistemaDesdeTxt();

    
    /**
     * Genera el menu de entrada
     * 
     * @param primaryStage
     * @return 
     */
    public GridPane getMenuLogin(Stage primaryStage) {

        
        menuLogin = new GridPane();
        menuLogin.setMinSize(600, 500);
        menuLogin.setVgap(5);
        menuLogin.setHgap(5);
        menuLogin.setPadding(new Insets(8, 8, 8, 8));
        menuLogin.setAlignment(Pos.CENTER);

        Text usrTxt = new Text("Usuario: ");
        TextField usrFld = new TextField();
        Text claveTxt = new Text("Clave: ");
        PasswordField claveFld = new PasswordField();

        Button login = new Button("Login");
        
        login.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent t) {
                
                String usuario = usrFld.getText();
                String clave = claveFld.getText();
               
                
                if(sistema.esLoginValido(usuario, clave)) {
                    
                    Empleado empleado = sistema.encuentraEmpleadoSegunUsuario(usuario);
                    System.out.println(empleado.toString());
                    System.out.println(((Cajero)empleado).aerolinea.vuelos);
                    if (empleado != null) {
                        
                        // Dependiendo del tipo de empleado muestra el menu que le toca
                        switch (empleado.getRol().toLowerCase()) {
                            case "a":
                                primaryStage.setScene(new Scene(getMenuAdmin(primaryStage)));
                                break;
                            case "c":
                                primaryStage.setScene(new Scene(getMenuCajero(primaryStage,empleado)));
                                break;
                            case "p":
                                primaryStage.setScene(new Scene(getMenuPlanificador(primaryStage,empleado)));
                                break;
                        }
                    }
                }
            }
        });
        
        Button salir = new Button("Salir");

        salir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                // Cierra el programa
                Stage stage = (Stage) salir.getScene().getWindow();
                stage.close();
            }
        });

        menuLogin.add(usrTxt, 0, 0);
        menuLogin.add(usrFld, 1, 0);
        menuLogin.add(claveTxt, 0, 1);
        menuLogin.add(claveFld, 1, 1);
        menuLogin.add(login, 1, 2);
        menuLogin.add(salir, 1, 3);

        return menuLogin;
    }
    
    

    public GridPane getMenuAdmin(Stage primaryStage) {
        
        menuAdmin = new GridPane();
        menuAdmin.setMinSize(600, 500);
        menuAdmin.setVgap(5);
        menuAdmin.setHgap(5);
        menuAdmin.setPadding(new Insets(8, 8, 8, 8));
        menuAdmin.setAlignment(Pos.CENTER);
        
        Label titulo = new Label("MENU ADMINISTRADOR");
        Button crearUsr = new Button("Crear Usuario");
        Button crearAerolinea = new Button("Crear Aerolinea");
        Button listarEmpleadosConsolaBtn = new Button("Listar Empleados a Consola");
        Button salir = new Button("Salir");
        Button logout = new Button("Logout");
        
        
        listarEmpleadosConsolaBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                Administrador.imprimirEmpleadorAConsola(sistema.getEmpleados());
            }
        });
        
        crearUsr.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                
                // Cambia al menu para ingresar los datos del nuevo usuario
                primaryStage.setScene(new Scene(getCrearUsr(primaryStage)));
            }
        });
        
        salir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                // Cierra el programa
                Stage stage = (Stage) salir.getScene().getWindow();
                stage.close();
            }
        });
        
        logout.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                
                // Regresa a la ventana anterior
                primaryStage.setScene(new Scene(getMenuLogin(primaryStage)));
            }
        });
        
        
        menuAdmin.add(titulo, 0, 0);
        menuAdmin.add(crearUsr, 0, 1);
        menuAdmin.add(crearAerolinea, 0, 2);
        menuAdmin.add(listarEmpleadosConsolaBtn,0,3);
        menuAdmin.add(logout, 0, 4);
        menuAdmin.add(salir, 0, 5);
        
        return menuAdmin;
        

    }
    
    private GridPane getCrearUsr (Stage primaryStage) {

        GridPane crearUsrPane = new GridPane();
        crearUsrPane.setMinSize(600, 500);
        crearUsrPane.setVgap(5);
        crearUsrPane.setHgap(5);
        crearUsrPane.setPadding(new Insets(8, 8, 8, 8));
        crearUsrPane.setAlignment(Pos.CENTER);

        Label tituloLabel = new Label("Crear Nuevo Usuario");
        
        Label cedulaLabel = new Label("Cedula: ");
        Label nombreLabel = new Label("Nombre: ");
        Label apellidoLabel = new Label("Apellido: ");
        Label emailLabel = new Label("Email: ");
        Label usrLabel = new Label("Nombre de Usuario: ");
        Label claveLabel = new Label("Clave: ");

        TextField cedulaFld = new TextField();
        TextField nombreFld = new TextField();
        TextField apellidoFld = new TextField();
        TextField emailFld = new TextField();
        TextField usrFld = new TextField();
        PasswordField claveFld = new PasswordField();
        
        Label rolLabel = new Label("Rol: ");
        ComboBox rolesCbox = new ComboBox();
        // Agrega los roles a mostrar
        rolesCbox.getItems().add("A");
        rolesCbox.getItems().add("C");
        rolesCbox.getItems().add("P");
        
        Label aerolineaLabel = new Label("Aerolinea: ");
        ComboBox<Aerolinea> aerolineasCbox = new ComboBox<>();
        // Agrega aerolineas dinamicamente
        ArrayList<Aerolinea> aerolineas = sistema.getAerolineas();
        for (Aerolinea aerolinea : aerolineas) {
            aerolineasCbox.getItems().add(aerolinea);
        }
        
        // Este msg solo se muestra al termina la transaccion
        // El msg a mostrar depende del resultado
        Label msgIngreso = new Label("");
        msgIngreso.setVisible(false);
        
        Button crearUsrBtn = new Button("Crear");
        Button regresarBtn = new Button("Regresar");

        regresarBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                
                primaryStage.setScene(new Scene(getMenuAdmin(primaryStage)));
            }
        });
        
        crearUsrBtn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent t) {
                
                Empleado e = null;
                
                String cedula = cedulaFld.getText();
                String nombre = nombreFld.getText();
                String apellido = apellidoFld.getText();
                String email = emailFld.getText();
                String usr = usrFld.getText();
                String clave = claveFld.getText();
                String rol = (String) rolesCbox.getValue();
                Aerolinea aerolinea = (Aerolinea) aerolineasCbox.getValue();
                String departamento = Administrador.asignaDepartamentoSegunRol(rol);
                
                boolean datosSonValidos = false;
                
                if(Administrador.esCedulaValida(cedula) &&
                        Administrador.esContraseñaValida(clave)) {
                    datosSonValidos = true;
                    clave = SeguridadFX.encriptar(claveFld.getText());
                }
                
                if(datosSonValidos) {
                    
                    switch (rol) {
                        case "A":
                            e = new Administrador(cedula, email, departamento, usr, clave, rol);
                            break;
                        case "C":
                            e = new Cajero(cedula, email, departamento, usr, clave, aerolinea, rol);
                            break;
                        case "P":
                            e = new Planificador(cedula, email, departamento, usr, clave, rol,aerolinea);
                            break;
                    }
                    
                    msgIngreso.setVisible(true);
                    sistema.agregarNuevoEmpleadoALista(e);
                    msgIngreso.setText("Empleado creado: " + e.getUsuario());
                    try {
                        sistema.guardarSistema();
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                    
                } else {
                    
                    msgIngreso.setVisible(true);
                    msgIngreso.setText("Error al ingresar datos!");
                }
            }
        });
        
        crearUsrPane.add(tituloLabel, 0, 0);
        crearUsrPane.add(cedulaLabel, 0, 1);
        crearUsrPane.add(cedulaFld, 1, 1);
        crearUsrPane.add(nombreLabel, 0, 2);
        crearUsrPane.add(nombreFld, 1, 2);
        crearUsrPane.add(apellidoLabel, 0, 3);
        crearUsrPane.add(apellidoFld, 1, 3);
        crearUsrPane.add(emailLabel, 0, 4);
        crearUsrPane.add(emailFld, 1, 4);
        crearUsrPane.add(usrLabel, 0, 5);
        crearUsrPane.add(usrFld, 1, 5);
        crearUsrPane.add(claveLabel, 0, 6);
        crearUsrPane.add(claveFld, 1, 6);
        crearUsrPane.add(rolLabel, 0, 7);
        crearUsrPane.add(rolesCbox, 1, 7);
        crearUsrPane.add(aerolineaLabel, 0, 8);
        crearUsrPane.add(aerolineasCbox, 1, 8);
        crearUsrPane.add(msgIngreso, 0, 9);
        crearUsrPane.add(crearUsrBtn, 0, 10);
        crearUsrPane.add(regresarBtn, 0, 11);
        
        return crearUsrPane;

    }
    
    public GridPane getMenuCajero (Stage primaryStage,Empleado empleado) {
        
        menuCajero = new GridPane();
        menuCajero.setMinSize(600, 500);
        menuCajero.setVgap(5);
        menuCajero.setHgap(5);
        menuCajero.setPadding(new Insets(8, 8, 8, 8));
        menuCajero.setAlignment(Pos.CENTER);
        
        Label titulo = new Label("MENU CAJERO");
        Button ventaBtn = new Button("Venta de Boletos");
        Button salir = new Button("Salir");
        Button logout = new Button("Logout");
        ListView lv = new ListView();
        
        salir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Stage stage = (Stage) salir.getScene().getWindow();
                stage.close();
            }
        });
        
        logout.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                primaryStage.setScene(new Scene(getMenuLogin(primaryStage)));
            }
        });
        
        
        menuCajero.add(titulo, 0, 0);
        menuCajero.add(ventaBtn, 0, 1);
        menuCajero.add(logout, 0, 2);
        menuCajero.add(salir, 0, 3);
                menuCajero.add(lv, 0, 2,2,2);
        
        GridPane.setHalignment(lv, HPos.CENTER);
        GridPane.setValignment(lv, VPos.CENTER);
        Thread t1 = new Thread(new PanelVuelos(((Cajero)empleado).aerolinea,lv));
        t1.start();
        return menuCajero;
    }
    
    public GridPane getMenuPlanificador(Stage primaryStage ,Empleado empleado) {
        
        menuPlanificador = new GridPane();
        menuPlanificador.setMinSize(600, 500);
        menuPlanificador.setVgap(5);
        menuPlanificador.setHgap(5);
        menuPlanificador.setPadding(new Insets(8, 8, 8, 8));
        menuPlanificador.setAlignment(Pos.CENTER);
        
        Label titulo = new Label("MENU PLANIFICADOR");
        Button planificarVueloBtn = new Button("Planificar Vuelo");
        Button ingresoAvionBtn = new Button("Ingresar Avion");
        Button salir = new Button("Salir");
        Button logout = new Button("Logout");
        ListView lv = new ListView();
        salir.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                Stage stage = (Stage) salir.getScene().getWindow();
                stage.close();
            }
        });
        
        logout.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                primaryStage.setScene(new Scene(getMenuLogin(primaryStage)));
            }
        });
        
        menuPlanificador.add(titulo, 0, 0);
        menuPlanificador.add(planificarVueloBtn, 0, 1);
        menuPlanificador.add(ingresoAvionBtn, 0, 2);
        menuPlanificador.add(logout, 0, 3);
        menuPlanificador.add(salir, 0, 4);
        menuPlanificador.add(lv, 0, 2,2,2);
        
        GridPane.setHalignment(lv, HPos.CENTER);
        GridPane.setValignment(lv, VPos.CENTER);
        Thread t1 = new Thread(new PanelVuelos(((Planificador)empleado).aerolinea,lv));
        
        return menuPlanificador;
        
    }
    
    // NO BORRAR!!!!!!
    private Sistema inicializarSistemaDesdeTxt(){
        
        Sistema s = new Sistema();
        s.actualizarDatosLeyendoTexto();
        try {
            s.guardarSistema();
            
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return s;
    }
    
    private Sistema inicializarSistemaConSerializacion() {
        
        Sistema s = null;
        
        try {
            s = Sistema.cargarSistema();
            return s;
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("ERROR: " + ex);
        }
        
        if(s == null)
            System.out.println("s es null");
        
        return s;
    }

}

package sistemaaeropuerto;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args){
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        PanelOrganizador paneOrganizer = new PanelOrganizador();
        GridPane loginRoot = paneOrganizer.getMenuLogin(primaryStage);
        Scene loginScreen = new Scene(loginRoot);
        
        
       
        primaryStage.setTitle("Sistema Aeropuerto");
        primaryStage.setScene(loginScreen);
        primaryStage.show();
    }

}
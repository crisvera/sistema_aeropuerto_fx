package sistemaaeropuerto;

import java.io.Serializable;
import static java.lang.Integer.parseInt;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Objects;
import java.util.Scanner;

/**
 *
 */
public class Vuelo implements Serializable {
     String codigo_de_vuelo, codigo_IATA1_arribo, codigo_IATA1_salida;
    LocalDate fecha_de_embarque, fecha_de_salida, fecha_de_arribo;
    LocalTime hora_de_embarque, hora_de_salida, hora_de_arribo;
    String puerta_de_arribo, puerta_de_salida;
    String categorias;
    String cadena;
    String lugar_de_arribo;
    Scanner scanner = new Scanner(System.in);
    String prueba;
    HashMap<String, Integer> asientos_disponibles;

    public Vuelo(String codigo, String codigo_IATA1_arribo, String codigo_IATA1_salida, LocalDate fecha_de_embarque, LocalDate fecha_de_salida, LocalDate fecha_de_arribo, LocalTime hora_de_embarque, LocalTime hora_de_arribo, LocalTime hora_de_salida, String puerta_de_salida, String puerta_de_arribo, String negocios,String economico, String lugar_de_arribo) {
        this.codigo_de_vuelo = codigo;
        this.codigo_IATA1_arribo = codigo_IATA1_arribo;
        this.codigo_IATA1_salida = codigo_IATA1_salida;
        this.fecha_de_embarque = fecha_de_embarque;
        this.fecha_de_salida = fecha_de_salida;
        this.fecha_de_arribo = fecha_de_arribo;
        this.hora_de_embarque = hora_de_embarque;
        this.hora_de_salida = hora_de_salida;
        this.hora_de_arribo = hora_de_arribo;
        this.puerta_de_arribo = puerta_de_arribo;
        this.puerta_de_salida = puerta_de_salida;
        this.lugar_de_arribo = lugar_de_arribo;
        this.asientos_disponibles = new HashMap();
        this.asientos_disponibles.put("negocios", parseInt(negocios));
        this.asientos_disponibles.put("economico", parseInt(economico));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.codigo_de_vuelo);
        hash = 59 * hash + Objects.hashCode(this.codigo_IATA1_arribo);
        hash = 59 * hash + Objects.hashCode(this.codigo_IATA1_salida);
        hash = 59 * hash + Objects.hashCode(this.fecha_de_embarque);
        hash = 59 * hash + Objects.hashCode(this.fecha_de_salida);
        hash = 59 * hash + Objects.hashCode(this.fecha_de_arribo);
        hash = 59 * hash + Objects.hashCode(this.hora_de_embarque);
        hash = 59 * hash + Objects.hashCode(this.hora_de_salida);
        hash = 59 * hash + Objects.hashCode(this.hora_de_arribo);
        hash = 59 * hash + Objects.hashCode(this.puerta_de_arribo);
        hash = 59 * hash + Objects.hashCode(this.puerta_de_salida);
        hash = 59 * hash + Objects.hashCode(this.categorias);
        hash = 59 * hash + Objects.hashCode(this.cadena);
        hash = 59 * hash + Objects.hashCode(this.lugar_de_arribo);
        hash = 59 * hash + Objects.hashCode(this.scanner);
        hash = 59 * hash + Objects.hashCode(this.prueba);
        hash = 59 * hash + Objects.hashCode(this.asientos_disponibles);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Vuelo other = (Vuelo) obj;
        if (!Objects.equals(this.codigo_de_vuelo, other.codigo_de_vuelo)) {
            return false;
        }
        if (!Objects.equals(this.codigo_IATA1_arribo, other.codigo_IATA1_arribo)) {
            return false;
        }
        if (!Objects.equals(this.codigo_IATA1_salida, other.codigo_IATA1_salida)) {
            return false;
        }
        if (!Objects.equals(this.puerta_de_arribo, other.puerta_de_arribo)) {
            return false;
        }
        if (!Objects.equals(this.puerta_de_salida, other.puerta_de_salida)) {
            return false;
        }
        if (!Objects.equals(this.categorias, other.categorias)) {
            return false;
        }
        if (!Objects.equals(this.cadena, other.cadena)) {
            return false;
        }
        if (!Objects.equals(this.lugar_de_arribo, other.lugar_de_arribo)) {
            return false;
        }
        if (!Objects.equals(this.prueba, other.prueba)) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_embarque, other.fecha_de_embarque)) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_salida, other.fecha_de_salida)) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_arribo, other.fecha_de_arribo)) {
            return false;
        }
        if (!Objects.equals(this.hora_de_embarque, other.hora_de_embarque)) {
            return false;
        }
        if (!Objects.equals(this.hora_de_salida, other.hora_de_salida)) {
            return false;
        }
        if (!Objects.equals(this.hora_de_arribo, other.hora_de_arribo)) {
            return false;
        }
        if (!Objects.equals(this.scanner, other.scanner)) {
            return false;
        }
        if (!Objects.equals(this.asientos_disponibles, other.asientos_disponibles)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Vuelo: " + codigo_de_vuelo + " IATA1_arribo: " + codigo_IATA1_arribo + " IATA1_salida: " + codigo_IATA1_salida + " fecha_de_embarque: " + fecha_de_embarque + " fecha_de_salida: " + fecha_de_salida + " fecha_de_arribo: " + fecha_de_arribo + " hora_de_embarque:" + hora_de_embarque + " hora_de_salida: " + hora_de_salida + " hora_de_arribo: " + hora_de_arribo + " puerta_de_arribo: " + puerta_de_arribo + " puerta_de_salida: " + puerta_de_salida + " lugar_de_arribo: " + lugar_de_arribo ;
    }

}

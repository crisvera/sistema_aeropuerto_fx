package sistemaaeropuerto;

import java.io.Serializable;

/**
 * Clase padre la cual tiene todos los atributos y metodos comunes entre
 * administrador , planificador y cajero.
 */
public class Empleado implements Serializable {

    private String identificador, correo, departamento, usuario, clave, rol;

    Empleado(String identificador, String correo, String departamento, String usuario, String clave, String rol) {
        this.correo = correo;
        this.departamento = departamento;
        this.identificador = identificador;
        this.usuario = usuario;
        this.clave = clave;
        this.rol = rol;
    }

    @Override
    public String toString() {
        return String.format("Clase: %-40s Usuario: %-15s Clave: %-15s Rol: %s", this.getClass(), usuario, clave, rol);
    }

    public String getIdentificador() {
        return identificador;
    }

    public String getCorreo() {
        return correo;
    }

    public String getDepartamento() {
        return departamento;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getClave() {
        return clave;
    }

    public String getRol() {
        return rol;
    }

}

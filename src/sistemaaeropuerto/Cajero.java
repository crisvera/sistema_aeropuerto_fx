package sistemaaeropuerto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import static java.lang.Integer.parseInt;
import java.util.ArrayList;

/**
 * Clase maneja todo los relacionado al cajero y la venta de boletos.
 */
public class Cajero extends Empleado {
    
    Aerolinea aerolinea;
    boolean menu_op;
    int valor_de_sc, boletos;
    ArrayList<String> destinos;
    ArrayList<String> categorias;
    ArrayList<Vuelo> vuelos_disponibles;
    final static String departamento = "comercial";
    private Vuelo vuelo_seleccionado;
    private String destino, categoria;
    // constructor del cajero 
    public Cajero(String identificador, String correo, String departamento, String usuario, String clave, Aerolinea aerolinea, String rol) {
        super(identificador, correo, departamento, usuario, clave, rol);
        this.aerolinea = aerolinea;
    }
     //MOSTRAR VUELO CREA LISTAS DE LOS POSIBLES DESTINOS Y LAS CLASES QUE DISPONEN LOS VUELOS 
    public void mostrar_vuelo() {
        //se usan metodos para extraer los destinos 
        destinos = new ArrayList();
        categorias = new ArrayList();
        vuelos_disponibles = new ArrayList();
       preguntar_destino();
    }
    //metodo que muestra los posibles destinos registrados en la aerolinea y pregunta por uno
    private void preguntar_destino() {
        for (Vuelo v : aerolinea.vuelos) {
            if (!destinos.contains(v.lugar_de_arribo)) {
                destinos.add(v.lugar_de_arribo);
            }
        }
    }

    //metodo que muestra las categorias de asientos de los aviones que tienen un destino preguntado 
    public void preguntar_categoria(String destino) {
        for (Vuelo v : aerolinea.vuelos) {
            if (destino.startsWith(v.lugar_de_arribo)) {
                for (String i : v.asientos_disponibles.keySet()) {
                    if (!categorias.contains(i)) {
                        categorias.add(i);
                    }
                }
            }
        }
        
    }

    public String[] mostrar_vuelos_filtrados(String categoria, String destino) { 
            ArrayList<String> str = new ArrayList();
            // se muestran los posibles VUELOS deacuerdo a la categoria y destino seleccionados
            for (Vuelo v : aerolinea.vuelos) {
                if (destino.startsWith(v.lugar_de_arribo)) {
                    str.add("VUELO:" + v.codigo_de_vuelo + ":: el numero disponible: " + v.asientos_disponibles.get(categoria) + " ");
                    vuelos_disponibles.add(v);
                    
                }
            }
            return GetStringArray(str);
    }
    
    public Vuelo buscar_vuelo_seleccionado(String vuelo_selecionado,String destino){
        String[] lista  = vuelo_selecionado.split(":");
        System.out.println(lista[1]);
        for (Vuelo v : aerolinea.vuelos) {
                if (destino.equalsIgnoreCase(v.lugar_de_arribo) && lista[1].equalsIgnoreCase(v.codigo_de_vuelo)) {
                    return v;
                }
            }
        return null;
    }
    public void ventan_compra(Cajero c,Vuelo vuelo_selecionado){
          System.out.println(c);
          System.out.println(vuelo_seleccionado);
    }
    
    public String[] getDestinos() {
       String[] str = GetStringArray(destinos); 
        return str;
    }
    public String[] getCategorias(){
        String[] str = GetStringArray(categorias);
        return str;
    }
    public static String[] GetStringArray(ArrayList<String> arr) 
    { 
  
        // declaration and initialise String Array 
        String str[] = new String[arr.size()]; 
  
        // ArrayList to Array Conversion 
        for (int j = 0; j < arr.size(); j++) { 
  
            // Assign each value to String array 
            str[j] = arr.get(j); 
        } 
  
        return str; 
    } 
    
    @Override
    public String toString() {
        return "Cajero :" + "identificador=" + this.getIdentificador() + ", correo=" + this.getCorreo() + ", departamento=" + this.getDepartamento() + '}';
    }
}

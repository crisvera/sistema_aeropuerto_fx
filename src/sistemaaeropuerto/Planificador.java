package sistemaaeropuerto;

/**
 *
 */
public class Planificador extends Empleado {

    Aerolinea aerolinea;

    public Planificador(String identificador, String correo, String departamento, String usuario, String clave, String rol, Aerolinea aerolinea) {
        super(identificador, correo, departamento, usuario, clave, rol);
        //se debe crear un metodo que llene los datos de la aerolinea
        this.aerolinea = aerolinea;
    }
}

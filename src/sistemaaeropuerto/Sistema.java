/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaaeropuerto;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * El sistema es la clase que contiene la informacion del programa. Esto incluye
 * pero no se limita a: lista de empleados, aerolineas, viajes.
 */
public class Sistema implements Serializable {

    private ArrayList<Empleado> empleados;
    private ArrayList<Aerolinea> aerolineas;
    // NO BORRAR!!!!!!
    public void actualizarDatosLeyendoTexto() {

        empleados = ProcesadorDeArchivos.cargarEmpleados();
        aerolineas = ProcesadorDeArchivos.cargarAerolineas();
    }

    /**
     * 
     * @param usr
     * @param clave es la clave que el usuario ingresa por pantalla, sin encriptar
     * @return 
     */
    public boolean esLoginValido(String usr, String clave) {

        clave = SeguridadFX.encriptar(clave);

        for (Empleado empleado : empleados) {

            if (empleado.getUsuario().equalsIgnoreCase(usr)
                    && empleado.getClave().equals(clave)) {
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * 
     * @param usr es el texto que representa el username de un empleado
     * @return un empleado en la lista de empleados con el mismo usuario, nulo
     * de lo contrario
     */
    public Empleado encuentraEmpleadoSegunUsuario(String usr) {
        
        for (Empleado empleado : empleados) {
            
            if (empleado.getUsuario().equals(usr))
                return empleado;
        }
        
        return null;
    }
    
    public void agregarNuevoEmpleadoALista(Empleado empleado) {
        
        empleados.add(empleado);
    }
    
    public ArrayList<Aerolinea> getAerolineas() {
        
        return aerolineas;
    }
    
    /**
     * Usa serializacion para guardar este objeto en un archivo binario.
     * 
     * @throws FileNotFoundException
     * @throws IOException 
     */
    public void guardarSistema() throws FileNotFoundException, IOException {

        try(ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("sistema.txt"))) {
            
            Sistema s = new Sistema();
            s.setAerolineas(this.aerolineas);
            s.setEmpleados(this.empleados);
            objectOutputStream.writeObject(s);
            objectOutputStream.flush();
            System.out.println("Se guardo el sistema por serializacion");
        }
    }
    
    /**
     * Usa serializacion para generar un sistema desde un archivo binario
     * @return el sistema mas recientemente grabado
     * @throws FileNotFoundException
     * @throws IOException
     * @throws ClassNotFoundException 
     */
    public static Sistema cargarSistema() throws FileNotFoundException, IOException, ClassNotFoundException {
        
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("sistema.txt"));) {
            System.out.println("Se va a cargar el sistema por serializacion");
            return (Sistema) objectInputStream.readObject();
        }
    }

    public void setEmpleados(ArrayList<Empleado> empleados) {
        this.empleados = empleados;
    }

    public void setAerolineas(ArrayList<Aerolinea> aerolineas) {
        this.aerolineas = aerolineas;
    }

    public ArrayList<Empleado> getEmpleados() {
        return empleados;
    }
    
    
    
}
